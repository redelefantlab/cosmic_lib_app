﻿namespace SwaggerAPI
{

    using System;
    using System.Data;
    using System.Threading.Tasks;
    using Npgsql;
    using study;

    public interface IConnectionFactory
    {
        IDbConnection Create();
        IDbConnection Create(string connectionString);
        Task<IDbConnection> CreateAsync();
        Task<IDbConnection> CreateAsync(string connectionString);
    }

    public class ConnectionFactory : IConnectionFactory
    {
        //private readonly IConnectionSetting _setting;
        private readonly string _settings;
        public ConnectionFactory(string settings)
        {
            this._settings = settings;
        }
        //public ConnectionFactory(IConnectionSetting setting)
        //{
        //    _setting = setting ?? throw new ArgumentNullException(nameof(setting));
        //    if (string.IsNullOrEmpty(_setting.ConnectionString))
        //    {
        //        throw new ArgumentOutOfRangeException(nameof(setting), "Connection string should not be null or empty");
        //    }


        //}

        public IDbConnection Create()
        {
            return Create(_settings);
        }

        public IDbConnection Create(string connectionString)
        {
            NpgsqlConnection dbConnection = new NpgsqlConnection(connectionString);
            dbConnection.Open();
            return dbConnection;
        }

        public async Task<IDbConnection> CreateAsync()
        {
            return await CreateAsync(_settings);
        }

        public async Task<IDbConnection> CreateAsync(string connectionString)
        {
            NpgsqlConnection dbConnection = new NpgsqlConnection(connectionString);
            await dbConnection.OpenAsync().ConfigureAwait(false);
            return dbConnection;
        }
    }
}
