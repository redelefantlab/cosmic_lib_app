using Microsoft.AspNetCore.Mvc;
using Dapper;
using study;

namespace SwaggerAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CosmicObjectController : ControllerBase
    {
        /// <summary>
        /// Gets the list of all CosmicObject.
        /// </summary>
        /// <returns>The list of CosmicObject.</returns>
        // GET: api/CosmicObject
        [HttpGet]
        public IEnumerable<CosmicObject> Get()
        {
            return GetCosmicObjects();
        }


        // GET: api/CosmicObject/5
        [HttpGet("{id}", Name = "Get")]
        public CosmicObject Get(int id)
        {
            return GetCosmicObjects().Find(e => e.Id == id);
        }


        /// <summary>
        /// Creates a CosmicObject.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST api/Employee
        ///     {        
        ///     Id = 1,
        ///     ObjName= "Earth",
        ///     Speed = 356,
        ///     Size = 789654      
        ///     }
        /// </remarks>
        /// <param name="cosmicobj"></param>
        // POST: api/CosmicObject
        [HttpPost]
        [Produces("application/json")]
        public void Post(int Id, string ObjName, int Speed, int Size)
        {
            var config = new ConfigurationBuilder()
               .AddJsonFile("appsettings.json")
               .Build();

            var settings = new AppSettings(config);

            CosmicObject cosmicObj = new CosmicObject() { Id = Id, ObjName = ObjName, Speed = Speed, Size = Size };

            ConnectionFactory factory = new ConnectionFactory(settings.ConnectionString);
            using (var connection = factory.Create())
            {
                var result = connection.Execute("INSERT INTO cosmicObj (Id , ObjName, Speed, Size) VALUES(@Id, @ObjName, @Speed)", Id);
            }

        }


        // PUT: api/CosmicObject/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] CosmicObject cosmicobj)
        {
            // Logic to update an CosmicObject
        }

        // DELETE: api/CosmicObject/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        private List<CosmicObject> GetCosmicObjects()
        {
            return new List<CosmicObject>()
        {
            new CosmicObject()
            {
                Id = 1,
                ObjName= "Earth",
                Speed = 356,
                Size = 789654
            },
            new CosmicObject()
            {
                Id = 2,
                ObjName= "Moon",
                Speed = 98,
                Size = 778
            }
        };
        }
    }
}