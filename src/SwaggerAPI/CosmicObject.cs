namespace SwaggerAPI
{
    public class CosmicObject
    {
        public int Id { get; set; }
        public string ObjName { get; set; }
        public int Speed { get; set; }
        public int Size { get; set; }
    }
}