using Cosmic_lib_app;
using NUnit.Framework;

namespace Test_Cosmic_lib_app
{
    [TestFixture]
    public class FileTests
    {
        private WriteFile _writeFile;
        private ReadFile _viewFile;
        private ReadFile _readFile;

        [SetUp]
        public void Setup()
        {
            _writeFile = new WriteFile();
            _viewFile = new ReadFile();
            _readFile = new ReadFile();
        }

        [Test]
        public void StartWriteTest()
        {
            var result = _writeFile.jsonStarFileWriter();
            Assert.IsNotNull(result);
        }

        [Test]
        public void jsonFileViewer()
        {
            var result = _viewFile.jsonFileViewer();
            Assert.IsNotNull(result);
        }

        [Test]
        public void jsonFileReader()
        {
            var result = _readFile.jsonFileReader();
            Assert.IsNotNull(result);
        }
    }
}