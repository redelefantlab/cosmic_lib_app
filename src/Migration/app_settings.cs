﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;

namespace DBMigrations
{
    internal class AppSettings : IConfigurationRoot
    {
        IDictionary<string, ConfigurationSection> _settings;

        public AppSettings()
        {
            _settings = new Dictionary<string, ConfigurationSection>();

            var connectionSring = new ConfigurationSection(this, null)
            {
                Value = "Server=localhost; User Id=postgres; Database=postgres;  Password=1234; Port=5432"
            };

            _settings["DBTEST"] = connectionSring;
        }
        
        public string this[string key]
        {
            get
            {
                return _settings[key].Value;
            }
            set
            {
               // _settings[key] = Value;
            }
        }

        public IEnumerable<IConfigurationProvider> Providers => throw new NotImplementedException();

        public IEnumerable<IConfigurationSection> GetChildren()
        {
            throw new NotImplementedException();
        }

        public IChangeToken GetReloadToken()
        {
            throw new NotImplementedException();
        }
        
        public IConfigurationSection GetSection(string key)
        {
            throw new NotImplementedException();
        }
        
        public void Reload()
        {
            throw new NotImplementedException();
        }
    }

}