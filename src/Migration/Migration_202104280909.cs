﻿using FluentMigrator;

namespace study
{
    [Migration(202104280909, "DKS-96")]
    public class Migration_202104280909 : Migration
    {
        public override void Up()
        {
            Create.Table("CosmicObject")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("ObjName").AsString(256)
                .WithColumn("Speed").AsDouble().Nullable()
                .WithColumn("Size").AsDouble().Nullable();

/*
            Create.Table("planet")
                .WithColumn("planetid").AsInt64().PrimaryKey().Identity()
                .WithColumn("name").AsString(256)
                .WithColumn("radius").AsString(256).Nullable()
                .WithColumn("size").AsDouble().Nullable()
                .WithColumn("speed").AsDouble().Nullable();

            Create.Table("star")
                .WithColumn("starid").AsInt64().PrimaryKey().Identity()
                .WithColumn("name").AsString(256)
                .WithColumn("radius").AsString(256).Nullable()
                .WithColumn("size").AsDouble().Nullable();

            Create.Table("asteroid")
                .WithColumn("asteroidid").AsInt64().PrimaryKey().Identity()
                .WithColumn("name").AsString(256)
                .WithColumn("radius").AsString(256).Nullable()
                .WithColumn("size").AsDouble().Nullable()
                .WithColumn("speed").AsDouble().Nullable();

            Create.Table("black_hole")
                .WithColumn("black_holeid").AsInt64().PrimaryKey().Identity()
                .WithColumn("name").AsString(256)
                .WithColumn("radius").AsString(256).Nullable()
                .WithColumn("size").AsDouble().Nullable();
*/
        }
        public override void Down()
        {
        }
    }
}
