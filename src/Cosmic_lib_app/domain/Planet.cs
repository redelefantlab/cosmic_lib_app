﻿
namespace study
{
    
    public class Planet
    {
        public int PlanetID { get; set; }

        private double speed;
        private string name;
        private int radius;
        private double size;

        public string Name
        {
            get { return name; }
            set { name = value; }

        }

        public int Radius
        {
            get { return radius; }
            set { radius = value; }

        }
        public double Size
        {
            get { return size; }
            set
            {
                bool success = false;
                while (!success)
                {
                    if (value <= 0)
                    {
                        Console.WriteLine($"{nameof(size)} указан в не допустимом диапазоне. Пожалуйста введите новое");
                        string str = Console.ReadLine();
                        success = double.TryParse(str, out value);
                        if (success)
                            size = value;
                    }
                    else
                    {
                        size = value;
                        success = true;
                    }
                }


            }


        }
        public double Speed_Planet
        {
            get { return speed; }
            set
            {
                bool success = false;
                while (!success)
                {
                    if (value <= 0)
                    {
                        Console.WriteLine($"{nameof(speed)} указан в не допустимом диапазоне. Пожалуйста введите новое");
                        string str = Console.ReadLine();
                        success = double.TryParse(str, out value);
                        if (success)
                            speed = value;
                    }
                    else
                    {
                        speed = value;
                        success = true;
                    }
                }

            }


        }
    }
}
