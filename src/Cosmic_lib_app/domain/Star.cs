﻿

namespace study
{
    
    public class Star
    {
        public int StarID { get; set; }
        private int distance_to_the_star;
        private string name;
        private int radius;
        private double size;

        public string Name
        {
            get { return name; }
            set { name = value; }

        }

        public int Radius
        {
            get { return radius; }
            set { radius = value; }

        }
        public double Size
        {
            get { return size; }
            set
            {
                bool success = false;
                while (!success)
                {
                    if (value <= 0)
                    {
                        Console.WriteLine($"{nameof(size)} указан в не допустимом диапазоне. Пожалуйста введите новое");
                        string str = Console.ReadLine();
                        success = double.TryParse(str, out value);
                        if (success)
                            size = value;
                    }
                    else
                    {
                        size = value;
                        success = true;
                    }
                }


            }


        }

        public int Distance_to_the_star
        {
            get { return distance_to_the_star; }
            set { distance_to_the_star = value; }
        }
    }
}
