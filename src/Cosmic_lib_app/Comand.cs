﻿namespace study
{
    public class Comand
    {
        DataBase _data;
        Addition _add;
        Deletion _del;
        Data _values;
        XmlMethods _xml;
        
        public Comand(DataBase database, Addition add, Deletion del, Data data, XmlMethods methods)
        {
            _data = database;
            _add = add;
            _del = del;
            _values = data;
            _xml = methods;
        }
        // Обработчик пользовательских команд
        public  void Comand_Handler(string key, List<object> cosmic_Objects)
        {

            if (key.Equals("Add Planet", StringComparison.OrdinalIgnoreCase))                                   // Добавлям Планету
            {
                object cosmic_Objects1 = null;
                var state = AddingState.Start;
                while (state != AddingState.Success)
                {
                    Console.WriteLine("Введите Название, Размер, Скорость и Радиус Планеты :");
                    string value = Console.ReadLine();
                    cosmic_Objects1 = _add.AddPlanet(value,out state);
                    if (state == AddingState.Fail)
                    {
                        Console.WriteLine("Неверно введены данные для {0}", key.Substring(4));
                    }
                    
                }
                cosmic_Objects.Add(cosmic_Objects1);
                _data.AddPlanet(cosmic_Objects1);

                Console.WriteLine("Объект добавлен!");
            }
            else if (key.Equals("Add Star", StringComparison.OrdinalIgnoreCase))                                 // Добавляем Звезду
            {
                object cosmic_Objects1 = null;
                var state = AddingState.Start;
                while (state != AddingState.Success)
                {
                    Console.WriteLine("Введите Название, Размер, Растояние до Звезды и ее Радиус :");
                    string value = Console.ReadLine();
                     cosmic_Objects1 = _add.AddStar(value, out state);
                    
                    if (state == AddingState.Fail)
                    {
                        Console.WriteLine("Неверно введены данные для {0}", key.Substring(4));
                    }
                    
                }
                cosmic_Objects.Add(cosmic_Objects1);
                _data.AddStar(cosmic_Objects1);
                // Сохраняем изменения в БД

                Console.WriteLine("Объект добавлен!");
            }
            else if (key.Equals("Add Asteroid", StringComparison.OrdinalIgnoreCase))                               // Добавляем Звезду
            {
                object cosmic_Objects1 = null;
                var state = AddingState.Start;
                while (state != AddingState.Success)
                {
                    Console.WriteLine("Введите Название, Размер, Скорость и Радиус Астероида :");
                    string value = Console.ReadLine();
                     cosmic_Objects1 = _add.AddAsteroid(value, out state);
                    
                    if (state == AddingState.Fail)
                    {
                        Console.WriteLine("Неверно введены данные для {0}", key.Substring(4));
                    }
                    
                }
                cosmic_Objects.Add(cosmic_Objects1);
                _data.AddAsteroid(cosmic_Objects1);
                // Сохраняем изменения в БД

                Console.WriteLine("Объект добавлен!");
            }
            else if (key.Equals("Add Black_hole", StringComparison.OrdinalIgnoreCase))                             // Добавляем Черную Дыру
            {
                object cosmic_Objects1 = null;
                var state = AddingState.Start;
                while (state != AddingState.Success)
                {
                    Console.WriteLine("Введите Название, Размер, Год Открытия и Радиус Черной дыры :");
                    string value = Console.ReadLine();
                     cosmic_Objects1 = _add.AddBlack_hole(value, out state);
                    
                    if (state == AddingState.Fail)
                    {
                        Console.WriteLine("Неверно введены данные для {0}", key.Substring(4));
                    }
                   
                }
                cosmic_Objects.Add(cosmic_Objects1);
                _data.AddBlack_hole(cosmic_Objects1);
                // Сохраняем изменения в БД

                Console.WriteLine("Объект добавлен!");
            }
            else if (key.Equals("Print", StringComparison.OrdinalIgnoreCase))                                         // Выводим на печать все объекты
            {
                cosmic_Objects = _data.Update(cosmic_Objects);
                Printer.PrintInfo(cosmic_Objects);
            }
            else if (key.Equals("Print Planet", StringComparison.OrdinalIgnoreCase) || key.Equals("Print Star", StringComparison.OrdinalIgnoreCase) ||
                 key.Equals("Print Asteroid", StringComparison.OrdinalIgnoreCase) || key.Equals("Print Black_hole", StringComparison.OrdinalIgnoreCase))
            {
                Printer.PrintCurrentType(cosmic_Objects, key);                                                      // Выводим на печать объекты по конкретному типу
            }
            else if (key.Equals("Serialize Planet", StringComparison.OrdinalIgnoreCase) || key.Equals("Serialize Star", StringComparison.OrdinalIgnoreCase) ||
                key.Equals("Serialize Asteroid", StringComparison.OrdinalIgnoreCase) || key.Equals("Serialize Black_hole", StringComparison.OrdinalIgnoreCase))
            {
                _xml.WriteToXml(cosmic_Objects, key);                                                         // Сериализует объекты определенного типа
            }
            else if (key.Equals("Desirialize", StringComparison.OrdinalIgnoreCase))
            {
                _xml.ReadToXml(ref cosmic_Objects);                                                           // Десириализует объекты из файла
                _data.Rewrite(cosmic_Objects);
            }
            else if (key.StartsWith("Delete", StringComparison.OrdinalIgnoreCase))
            {
                var state = AddingState.Start;                                                                      // Удаляет определенный объект из БД и листа
                state = _del.DeleteObj(key, cosmic_Objects);
                if (state == AddingState.Fail)
                {
                    Console.WriteLine($"Объекта с таким Id нет или неверно введены данные!");
                }
                if (state == AddingState.Success)
                    _data.DeleteObj(key);
                Console.WriteLine("Объект удален!");
            }
            else if (key.StartsWith("Back up", StringComparison.OrdinalIgnoreCase))                                  // Сбрасывает значения и восстанавливает изначально 
            {                                                                                                        // предустановленные данные
                 _values.CreateData(cosmic_Objects);
                _data.Rewrite(cosmic_Objects);
                cosmic_Objects = _data.Update(cosmic_Objects);
                Console.WriteLine("Система восстановлена!");
            }
            else
                Console.WriteLine("Неверно введена команда. Повторите снова:");
        }
    }
}