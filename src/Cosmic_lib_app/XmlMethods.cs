﻿using System.Xml.Serialization;

namespace study
{
    
     public class XmlMethods
    {
         Type[] types = new Type[] { typeof(Black_hole), typeof(Planet), typeof(Star), typeof(Asteroid) };
        Data _values;
        public XmlMethods(Data data)
        {
            _values = data;
        }

        //Создает Xml файл с предустановленными данными
         public void CreateDataInXmlFile()
        {
            List<object> list = _values.SomeSpaceObjects();
      

            var xmlSerializer = new XmlSerializer(typeof(List<object>), types);

            using FileStream fs = new FileStream(@"Cosmic.xml", FileMode.Create);
            xmlSerializer.Serialize(fs, list);
        }
        // Записывает объекты в XML файл
         public void WriteToXml(List<object> cosmic_Objects, string str)
        {
            str = str.Remove(0, 10);
            var xmlSerializer = new XmlSerializer(typeof(List<object>), types);
            List<object> buffer = new List<object>();
            for (int i = 0; i < cosmic_Objects.Count; i++)
            {
                if (str.Equals("Planet", StringComparison.OrdinalIgnoreCase))
                {
                    if (cosmic_Objects[i] is Planet)
                    {
                        buffer.Add((Planet)cosmic_Objects[i]);
                    }
                }
                else if (str.Equals("Star", StringComparison.OrdinalIgnoreCase))
                {
                    if (cosmic_Objects[i] is Star)
                    {
                        buffer.Add((Star)cosmic_Objects[i]);
                    }
                }
                else if (str.Equals("Asteroid", StringComparison.OrdinalIgnoreCase))
                {
                    if (cosmic_Objects[i] is Asteroid)
                    {
                        buffer.Add((Asteroid)cosmic_Objects[i]);
                    }
                }
                else if (str.Equals("Black_hole", StringComparison.OrdinalIgnoreCase))
                {
                    if (cosmic_Objects[i] is Black_hole)
                    {
                        buffer.Add((Black_hole)cosmic_Objects[i]);
                    }
                }

            }
            using (FileStream fs = new FileStream(@"Cosmic.xml", FileMode.Create))
            {
                xmlSerializer.Serialize(fs, buffer);

            }
            Console.WriteLine("Cериализация прошла успешно!");
        }
        //Читает из XML файла и записывает в БД
         public void ReadToXml(ref List<object> cosmic_Objects)
        {
            bool state_1 = false, state_2 = false, state_3 = false, state_4 = false;

            var xmlSerializer = new XmlSerializer(typeof(List<object>), types);

            using (FileStream fs = new FileStream(@"Cosmic.xml", FileMode.OpenOrCreate, FileAccess.Read))
            {

                List<object> buffer = xmlSerializer.Deserialize(fs) as List<object>;
                if (buffer != null)
                {
                    foreach (object obj in buffer)
                    {
                        if (obj is Planet) state_1 = true;
                        if (obj is Star) state_2 = true;
                        if (obj is Asteroid) state_3 = true;
                        if (obj is Black_hole) state_4 = true;

                    }
                }
                if (state_1) cosmic_Objects.RemoveAll(x => x is Planet);
                if (state_2) cosmic_Objects.RemoveAll(x => x is Star);
                if (state_3) cosmic_Objects.RemoveAll(x => x is Asteroid);
                if (state_4) cosmic_Objects.RemoveAll(x => x is Black_hole);
                cosmic_Objects.AddRange(buffer);
                Console.WriteLine("Десериализация прошла успешно!");
            }
        }
    }
}