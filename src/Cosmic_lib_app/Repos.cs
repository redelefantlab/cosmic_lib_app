﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace study
{
    public class Repos
    {
        private AppSettings _settings;
        public Repos(AppSettings settings)
        {
            _settings = settings;
        }
        public void Execute(string sql, object param )
        {
            ConnectionFactory factory = new ConnectionFactory(_settings.ConnectionString);
            using (var connection = factory.Create())
            {
                var result = connection.Execute(sql, param);
            }
        }
        public void Execute(string sql)
        {
            ConnectionFactory factory = new ConnectionFactory(_settings.ConnectionString);
            using (var connection = factory.Create())
            {
                var result = connection.Execute(sql);
            }
        }
        public IEnumerable<T> Query<T>(string sql)
        {
            ConnectionFactory factory = new ConnectionFactory(_settings.ConnectionString);
            using (var connection = factory.Create())
            {
                var result = connection.Query<T>(sql);
                return result;
            }
        }
    }
}
