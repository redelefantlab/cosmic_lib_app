﻿namespace study
{
     public class Addition
    {

        // Метод создает объект Планета и возвращает ее
         public object AddPlanet(string str, out AddingState state)
        {
            string[] parameters = str.Split(" ");
            if (parameters.Length < 4)
            {
                state = AddingState.Fail;
                return  null;
            }
            Planet planet = new Planet();
            planet.Name = parameters[0];
            if (!double.TryParse(parameters[1], out double size))
            {
                state = AddingState.Fail;
                return  null;
            }
            planet.Size = size;

            if (!double.TryParse(parameters[2], out double speed))
            {
                state = AddingState.Fail;
                return  null;
            }
            planet.Speed_Planet = speed;
            if (!int.TryParse(parameters[3], out int radius))
            {
                state = AddingState.Fail;
                return  null;
            }
            planet.Radius = radius;
            state = AddingState.Success;
            return  planet;
        }

        // Метод создает объект Звезда и возвращает ее
        public object AddStar(string str, out AddingState state)
        {
            string[] parameters = str.Split(" ");
            if (parameters.Length < 4)
            {
                state = AddingState.Fail;
                return null;
            }
            Star star = new Star();
            star.Name = parameters[0];
            if (!double.TryParse(parameters[1], out double size))
            {
                state = AddingState.Fail;
                return null;
            }
            star.Size = size;

            if (!int.TryParse(parameters[2], out int distance_to_the_star))
            {
                state = AddingState.Fail;
                return null;
            }
            star.Distance_to_the_star = distance_to_the_star;
            if (!int.TryParse(parameters[3], out int radius))
            {
                state = AddingState.Fail;
                return null;
            }
            star.Radius = radius;
            state = AddingState.Success;
            return star;
        }

        // Метод создает объект Астероид и возвращает ее
        public object AddAsteroid(string str, out AddingState state)
        {
            string[] parameters = str.Split(" ");
            if (parameters.Length < 4)
            {
                state = AddingState.Fail;
                return null;
            }
            Asteroid asteroid = new Asteroid();
            asteroid.Name = parameters[0];
            if (!double.TryParse(parameters[1], out double size))
            {
                state = AddingState.Fail;
                return null;
            }
            asteroid.Size = size;

            if (!double.TryParse(parameters[2], out double speed))
            {
                state = AddingState.Fail;
                return null;
            }
            asteroid.Speed_Asteroid = speed;
            if (!int.TryParse(parameters[3], out int radius))
            {
                state = AddingState.Fail;
                return null;
            }
            asteroid.Radius = radius;
            state = AddingState.Success;
            return asteroid;
        }

        // Метод создает объект  Черная Дыра и возвращает ее
        public object AddBlack_hole(string str, out AddingState state)
        {
            string[] parameters = str.Split(" ");
            if (parameters.Length < 4)
            {
                state = AddingState.Fail;
                return null;
            }
            Black_hole black_hole = new Black_hole();
            black_hole.Name = parameters[0];

            if (!double.TryParse(parameters[1], out double size))
            {
                state = AddingState.Fail;
                return null;
            }
            black_hole.Size = size;

            if (!int.TryParse(parameters[2], out int year))
            {
                state = AddingState.Fail;
                return null;
            }

            if (!int.TryParse(parameters[3], out int radius))
            {
                state = AddingState.Fail;
                return null;
            }
            black_hole.Radius = radius;
            state = AddingState.Success;
            return black_hole;
        }
    }
    public enum AddingState
    {
        Start,
        Success,
        Fail
    }


}
