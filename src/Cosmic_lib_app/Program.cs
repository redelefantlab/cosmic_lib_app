﻿using Microsoft.Extensions.Configuration;

// Cоздать AppSettings и поместить в него настройки, как у меня
// Создать репозиторий который видит объект AppSettings, через конструктор добавить , так же в него закинуть методы, которые отвечают за какую то работу непосредственно с Базой данных.
// У меня это Execute и Query
// Так же файл json с сылкой на бд закинуть в оснвной проект, настроить чтобы App Settings считывал данные с него
// В DataBase назначить поле Репозитория, через конструктор добавить. Затем все новые классы подключить через main, в обработчике команд использовать функции из репозитория теперь.
//Все методы , у которых ссылка на Бд прописана руками, изменить, чтобы считывали с класса AppSettings
// P.s Статических классов и функций в проекте быть не должно
namespace study
{
    class Program
    {
        static void Main()
        {
            var config = new ConfigurationBuilder()
               .AddJsonFile("appsettings.json")
               .Build();
            var settings = new AppSettings(config);
            var repos = new Repos(settings);
            var values = new Data();
            var add = new Addition();
            var del = new Deletion();
            var Xml = new XmlMethods(values);
            var db = new DataBase(repos);
            var comand = new Comand(db, add, del, values, Xml);
           
            // Предустанавливаем значения в Xml Файл
            Xml.CreateDataInXmlFile();

            //Cоздаем List 
            List<object> cosmic_Objects = new List<object>();

            // Считываем данные с БД и записываем в List объекты
            cosmic_Objects = db.Update(cosmic_Objects);
            
            //Выводим на печать объекты и их значения с базы данных
            Printer.PrintInfo(cosmic_Objects);

            bool working = true;
            // Обработчик команд
            while (working)
            {
                Console.WriteLine("Введите команду:");
                string key = Console.ReadLine();
                
                comand.Comand_Handler(key, cosmic_Objects);
            }
 
        }

    }
}
